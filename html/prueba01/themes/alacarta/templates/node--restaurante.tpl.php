
<div class="conteiner-fluid p-1"> 
<h2 class="background p-4"> WELCOME TO MY RESTAURANT</h2>

    <div class="row a">
        <div class="col-12 mt-1">
            <h1><?php echo $title; ?></h1>
        </div>
    </div>

    <div class="row b"> 

                <div class="col-5 mt-3">
                    <?php echo render( $content['field_logo']); ?>
                </div>
                <div class="col-5 mt-3">
                    <?php echo render( $content['field_ruc']); ?>
                </div>          

    </div>
    

</div>